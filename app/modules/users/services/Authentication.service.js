(function() {
    'use strict';
    angular.module('users').service('Authentication', [
        '$http',
        '$q',
        '$rootScope',
        '$state',
        'Config',
        'growl',
        'localStorageService',
        'requestHuella',
        function(
            $http,
            $q,
            $rootScope,
            $state,
            Config,
            growl,
            localStorageService,
            requestHuella
        ) {
            var _this = this;
            var setAuthenticationData = function (user, tokenId) {
                _this.user = user;
                _this.accessToken = tokenId;

                localStorageService.set('user', user);
                localStorageService.set('access_token', tokenId);

                if($rootScope.returnToState){
                    $state.go($rootScope.returnToState);
                }else{
                    $state.go('app.home');
                }

                $rootScope.$broadcast('autoLogIn');
            };
            var unsetAuthenticationData = function() {
                _this.user = null;
                _this.accessToken = null;
                localStorageService.remove('user', 'access_token');
                $state.go('login');
            };

            _this.user = localStorageService.get('user');
            _this.accessToken = localStorageService.get('access_token');

            this.login = function(_credentials) {
                requestHuella.huella().then(function(resp){
                    if(resp.status === '-1') {
                        growl.warning(resp.error, {ttl: 3000});
                    }else{
                        // Consumir Api interna de login, en el succes de la promesa
                        // Enviar los datos al metodo setAuthenticationData()
                        // Que espera dos parametros (usuario, token);
                    }
                }, function(error) {
                    growl.error(error.error, {ttl: 3000});
                });
            };

            this.logout = function() {
                unsetAuthenticationData();

            };
            this.isAuth = function() {
                if (localStorageService.get('access_token')) {
                    return true;
                } else {
                    return false;
                }
            };
            this.currentUser = function() {
                return localStorageService.get('user');
            };
        }
    ]);
})();
