(function() {
  'use strict';
  angular.module('home').controller('Home', ['$scope', '$http','$q','$anchorScroll','$rootScope','$timeout','$cookieStore','$location',
  function($scope, $http, $q,$anchorScroll,$rootScope,$timeout,$cookieStore,$location){
    var vmHome = this;

    vmHome.radioValue = 3;

    vmHome.globales = [];
    if($cookieStore.get('globals')){
      vmHome.btnenabled = true;
      vmHome.globales = $cookieStore.get('globals');
    };

    //Asigna el user anonimo cuando esta deslogueado
    var username = "";
    if(vmHome.globales.length == 0){
      username = "anonimo";
    }else{
      username = vmHome.globales.currentUser.username;
    }
    //url: 'http://gateway.marvel.com/v1/public/characters?ts=1&apikey=cf6164ac2d6e7003dcd3f6a53520122e&hash=5166f2856433046d8e37d1cad56f2fbd'
            
    var publicKey = 'f1da2ae2dc487b462dc04513dea9eac1';
    var baseUrl = 'http://gateway.marvel.com/v1/';
    var limit = 50;

    var urlService = 'http://qa-apipos.coppel.com:9000/planlealtad/api/v1/marvel_service';



    vmHome.offset = 0;
    vmHome.busy = false;
    vmHome.characteres = [];

    //Funcion que consulta los personajes leidos
    var isRead = function() {
      return $http({
        method: 'POST',
        url: urlService,
        data: { "opcion": 1}
        });
    };
    
    //Funcion que consulta los personajes leidos por usuario
    var isReadByUser = function(username){
      return $http({
        method: 'POST',
        url: urlService,
        data: { "opcion": 1,"user":username}
        });
    }

    //Funcion que guarda los personajes al ser leidos
    var saveRead = function(id) {
      return $http({
        method: 'POST',
        url: urlService,
        data: { "opcion": 2, "id_character":id, "user":username}
      });
    };

    //Funcion que guarda los personajes con usuario al ser leidos
    var saveReadLogin = function(id,username) {
      return $http({
        method: 'POST',
        url: urlService,
        data: { "opcion": 4, "id_character":id, "user":username}
      });
    };

    //Asigna los personajes leidos al estar deslogueado al usuario 
    if(username != "anonimo"){
      isReadByUser("anonimo").then(function(data) {
        for (let i = 0; i < data.data.data.response.length; i++) {
          saveReadLogin(data.data.data.response[i].id_character,username).then(function(response) {
          });
        };
      });
    }

    //Funcion para consultar los usuarios
    var loginService = function(username,password) {
      return $http({
        method: 'POST',
        url: urlService,
        data: { "opcion": 3, "user":username, "pass":password}
      });
    };    

    //Funcion para consultar los personajes
    var getCharacteres = function(){
      return $http({
        method: 'GET',
        url: baseUrl + 'public/characters?limit='+ limit +'&apikey=' + publicKey
      }).then(function successCallback(response) {
        return response;
      }, function errorCallback(response) {
        alert('Error al consumir la api!')
      });
    };

    //Funcion que consulta los personajes por id
    var getCharacteresById = function(id){
      return $http({
        method: 'GET',
        url: baseUrl+'public/characters/' + id +'?apikey=' + publicKey
      }).then(function successCallback(response) {
        return response;
      }, function errorCallback(response) {
        alert('Error al consumir la api!')
      });
    };

    //Funcion que consulta los personajes siguientes al scrollear la pagina
    var findNext = function(offset) {
      var url = baseUrl + 'public/characters?limit=' + limit +'&offset=' + (limit*offset) + '&apikey=' + publicKey;      
      return $http({
        method: 'GET',
        url: url,
      }).then(function successCallback(response) {
        return response;
      }, function errorCallback(response) {
        alert('Error al consumir la api!')
      });
    };

    vmHome.closemodal = function (radioValue) {
      if(radioValue == 1){
        vmHome.reads();
      }else if(radioValue == 2){
        vmHome.noreads();
        $anchorScroll();
      }else if(radioValue == 3){
        vmHome.reloadData();
        $anchorScroll();
      }else if(radioValue == undefined){
        vmHome.reloadData();
      }
  };

    getCharacteres().then(function(response) {
      vmHome.characteres = [];
      vmHome.busy = false;
      vmHome.characteres = response.data.data.results;
      isRead().then(function(data) {
        for (let i = 0; i < response.data.data.results.length; i++) {
          for (let j = 0; j < data.data.data.response.length; j++) {
            if(response.data.data.results[i].id == data.data.data.response[j].id_character){
              if(username != "anonimo"){
                vmHome.characteres[i].is_read = data.data.data.response[j].is_read;
                vmHome.characteres[i].is_read = "Visto"
              }
            }
          }
        }
      });
    });

    //Funcion que consulta los personajes siguientes al scrollear la pagina
    vmHome.load = function() {
      if(vmHome.busy) {
        return;
      }
      vmHome.busy = true;
      findNext(vmHome.offset).then(function(response) {
        var chars = response.data.data.results;
        chars.forEach(function(item) {
          vmHome.characteres.push(item);
        }.bind(vmHome));
        vmHome.offset++;
        vmHome.busy = false;
      }.bind(vmHome));
    }.bind(vmHome);

    //Funcion que guarda un personaje al ser leido
    vmHome.IdCharacter = function(id,username) {
      saveRead(id,username).then(function() {
      });
      getCharacteresById(id).then(function(response){
        vmHome.characteresmodal = response.data.data.results;
      });
    };

    //Funcion que consulta los personajes leidos por usuarios
    vmHome.reads = function(){
      vmHome.characteres = [];
      vmHome.characteresreads = [];
      isReadByUser(username).then(function(data) {
        for (let j = 0; j < data.data.data.response.length; j++) {
          getCharacteresById(data.data.data.response[j].id_character).then(function(response){
            for (let i = 0; i < response.data.data.results.length; i++) {
              vmHome.characteresreads.push(response.data.data.results[i]);
            }
          });
        }
      });
      vmHome.busy = true;
      vmHome.characteres = vmHome.characteresreads;
    }

    //Funcion que consulta los personajes no leidos por usuarios
    vmHome.noreads = function(){
      vmHome.busy = false;
      getCharacteres().then(function(response) {
        isReadByUser(username).then(function(data) {
          for( var i=response.data.data.results.length - 1; i>=0; i--){
            for( var j=0; j<data.data.data.response.length; j++){
              if(response.data.data.results[i] && (response.data.data.results[i].id === data.data.data.response[j].id_character)){
                response.data.data.results.splice(i, 1);
              }
            }
         }
         vmHome.characteres = response.data.data.results;
        });
      });
    }

    vmHome.reloadData = function(){
      vmHome.characteres = [];
      vmHome.busy = false;
      getCharacteres().then(function(response) {
        vmHome.characteres = response.data.data.results;
        isRead().then(function(data) {
          for (let i = 0; i < response.data.data.results.length; i++) {
            for (let j = 0; j < data.data.data.response.length; j++) {
              if(response.data.data.results[i].id == data.data.data.response[j].id_character){
                if(username != "anonimo"){
                  vmHome.characteres[i].is_read = data.data.data.response[j].is_read;
                  vmHome.characteres[i].is_read = "Visto"
                }
              }
            }
          }
        });
      });
    }

    //Funcion que asigna las credenciales al loguearte 
    var SetCredentials = function (username, password) {
      var authdata = username + ':' + password;
      $rootScope.globals = {
          currentUser: {
              username: username,
              password: password
          }
      };
      $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
      $cookieStore.put('globals', $rootScope.globals);
      vmHome.userlogin = $cookieStore.get('globals');
    };

    //Funcion para hacer el login y asignar las credenciales
    vmHome.login = function (username, password) {
      vmHome.dataLoading = true;
      loginService(vmHome.username,vmHome.password).then(function(response){
        if(response.data.meta.status == "SUCCESS"){
          SetCredentials(vmHome.username, vmHome.password);
          location.reload();   
        }else if (response.data.meta.status =="ERROR"){
          swal("Usuario o contraseña incorrecto!");
        }
      });
    };

    //Funcion para desloguearte
    vmHome.logout = function(){
      $rootScope.globals = {};
      $cookieStore.remove('globals');
      $http.defaults.headers.common.Authorization = 'Basic ';
      location.reload(); 
    };
  }
  ]);
})();
