(function() {
    'use strict';
    angular.module('home').controller('HomeLogin', ['$scope', '$http','$q','$anchorScroll','$rootScope','$timeout','$cookieStore','$location',
    function($scope, $http, $q,$anchorScroll,$rootScope,$timeout,$cookieStore,$location){
      var vmHome = this;
      
  
      //url: 'http://gateway.marvel.com/v1/public/characters?ts=1&apikey=cf6164ac2d6e7003dcd3f6a53520122e&hash=5166f2856433046d8e37d1cad56f2fbd'
              
      var publicKey = 'f1da2ae2dc487b462dc04513dea9eac1';
      var baseUrl = 'http://gateway.marvel.com/v1/';
      var limit = 50;
  
      var urlService = 'http://qa-apipos.coppel.com:9000/planlealtad/api/v1/marvel_service';
  

  
      var Login = function (username, password, callback) {
        console.log(username);
        console.log(password);
        /* Dummy authentication for testing, uses $timeout to simulate api call
         ----------------------------------------------*/
        $timeout(function () {
            var response = { success: username === 'test' && password === 'test' };
            if (!response.success) {
                response.message = 'Username or password is incorrect';
            }
            callback(response);
        }, 1000);
  
  
        /* Use this for real authentication
         ----------------------------------------------*/
        //$http.post('/api/authenticate', { username: username, password: password })
        //    .success(function (response) {
        //        callback(response);
        //    });
  
    };
  
  
   var SetCredentials = function (username, password) {
      var authdata = username + ':' + password;
      console.log(authdata);
      $rootScope.globals = {
          currentUser: {
              username: username,
              authdata: authdata
          }
      };
      $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
      $cookieStore.put('globals', $rootScope.globals);
  };
  
  var ClearCredentials = function () {
      $rootScope.globals = {};
      $cookieStore.remove('globals');
      $http.defaults.headers.common.Authorization = 'Basic ';
  };  
  
  
  
      vmHome.login = function () {
        vmHome.dataLoading = true;
        Login(vmHome.username, vmHome.password, function (response) {
          console.log(response);
            if (response.success) {
                SetCredentials(vmHome.username, vmHome.password);
                location.reload(); 
  
            } else {
                vmHome.error = response.message;
                vmHome.dataLoading = false;
            }
        });
    };
  
  
  
  
  
  
  
    }
    ]);
  })();
  