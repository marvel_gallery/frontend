/**
 <b>Ace custom scroller</b>. It is not as feature-rich as plugins such as NiceScroll but it's good enough for most cases.
*/
(function($ , undefined) {
  var Ace_Scroll = function(element , _settings) {
    var self = this;
    
    var attrib_values = ace.helper.getAttrSettings(element, $.fn.ace_scroll.defaults);
    var settings = $.extend({}, $.fn.ace_scroll.defaults, _settings, attrib_values);
  
    this.size = 0;
    this.lock = false;
    this.lock_anyway = false;
    
    this.$element = $(element);
    this.element = element;
    
    var vertical = true;

    var disabled = false;
    var active = false;
    var created = false;

    
    var $content_wrap = null, content_wrap = null;
    var $track = null, $bar = null, track = null, bar = null;
    var bar_style = null;
    
    var bar_size = 0, bar_pos = 0, bar_max_pos = 0, bar_size_2 = 0, move_bar = true;
    var reset_once = false;
    
    var styleClass = '';
    var trackFlip = false;//vertical on left or horizontal on top
    var trackSize = 0;

    var css_pos,
      css_size,
      max_css_size,
      client_size,
      scroll_direction,
      scroll_size;

    var ratio = 1;
    var inline_style = false;
    var mouse_track = false;
    var mouse_release_target = 'onmouseup' in window ? window : 'html';
    var dragEvent = settings.dragEvent || false;
    
    var trigger_scroll = _settings.scrollEvent || false;
    
    
    var detached = settings.detached || false;//when detached, hideOnIdle as well?
    var updatePos = settings.updatePos || false;//default is true
    
    var hideOnIdle = settings.hideOnIdle || false;
    var hideDelay = settings.hideDelay || 1500;
    var insideTrack = false;//used to hide scroll track when mouse is up and outside of track
    var observeContent = settings.observeContent || false;
    var prevContentSize = 0;
    
    var is_dirty = true;//to prevent consecutive 'reset' calls
    
    this.ref = function() {
      return this;
    }
    
    this.create = function(_settings) {
      if(created) return;

      if(_settings) settings = $.extend({}, $.fn.ace_scroll.defaults, _settings);

      this.size = parseInt(this.$element.attr('data-size')) || settings.size || 200;
      vertical = !settings['horizontal'];

      css_pos = vertical ? 'top' : 'left';//'left' for horizontal
      css_size = vertical ? 'height' : 'width';//'width' for horizontal
      max_css_size = vertical ? 'maxHeight' : 'maxWidth';

      client_size = vertical ? 'clientHeight' : 'clientWidth';
      scroll_direction = vertical ? 'scrollTop' : 'scrollLeft';
      scroll_size = vertical ? 'scrollHeight' : 'scrollWidth';


      this.$element.addClass('ace-scroll');
      if(this.$element.css('position') == 'static') {
        inline_style = this.element.style.position;
        this.element.style.position = 'relative';
      } else inline_style = false;

      var scroll_bar = null;
      if(!detached) {
        this.$element.wrapInner('<div class="scroll-content" />');
        this.$element.prepend('<div class="scroll-track"><div class="scroll-bar"></div></div>');
      }
      else {
        scroll_bar = $('<div class="scroll-track scroll-detached"><div class="scroll-bar"></div></div>').appendTo('body');
      }


      $content_wrap = this.$element;
      if(!detached) $content_wrap = this.$element.find('.scroll-content').eq(0);
      
      if(!vertical) $content_wrap.wrapInner('<div />');
      
      content_wrap = $content_wrap.get(0);
      if(detached) {
        //set position for detached scrollbar
        $track = scroll_bar;
        setTrackPos();
      }
      else $track = this.$element.find('.scroll-track').eq(0);
      
      $bar = $track.find('.scroll-bar').eq(0);
      track = $track.get(0);
      bar = $bar.get(0);
      bar_style = bar.style;

      //add styling classes and horizontalness
      if(!vertical) $track.addClass('scroll-hz');
      if(settings.styleClass) {
        styleClass = settings.styleClass;
        $track.addClass(styleClass);
        trackFlip = !!styleClass.match(/scroll\-left|scroll\-top/);
      }
      
      //calculate size of track!
      if(trackSize == 0) {
        $track.show();
        getTrackSize();
      }
      
      $track.hide();
      

      //if(!touchDrag) {
      $track.on('mousedown', mouse_down_track);
      $bar.on('mousedown', mouse_down_bar);
      //}

      $content_wrap.on('scroll', function() {
        if(move_bar) {
          bar_pos = parseInt(Math.round(this[scroll_direction] * ratio));
          bar_style[css_pos] = bar_pos + 'px';
        }
        move_bar = false;
        if(trigger_scroll) this.$element.trigger('scroll', [content_wrap]);
      })


      if(settings.mouseWheel) {
        this.lock = settings.mouseWheelLock;
        this.lock_anyway = settings.lockAnyway;

        //mousewheel library available?
        this.$element.on(!!$.event.special.mousewheel ? 'mousewheel.ace_scroll' : 'mousewheel.ace_scroll DOMMouseScroll.ace_scroll', function(event) {
          if(disabled) return;
          checkContentChanges(true);

          if(!active) return !self.lock_anyway;

          if(mouse_track) {
            mouse_track = false;
            $('html').off('.ace_scroll')
            $(mouse_release_target).off('.ace_scroll');
            if(dragEvent) self.$element.trigger('drag.end');
          }
          

          event.deltaY = event.deltaY || 0;
          var delta = (event.deltaY > 0 || event.originalEvent.detail < 0 || event.originalEvent.wheelDelta > 0) ? 1 : -1
          var scrollEnd = false//have we reached the end of scrolling?
          
          var clientSize = content_wrap[client_size], scrollAmount = content_wrap[scroll_direction];
          if( !self.lock ) {
            if(delta == -1) scrollEnd = (content_wrap[scroll_size] <= scrollAmount + clientSize);
            else scrollEnd = (scrollAmount == 0);
          }

          self.move_bar(true);

          //var step = parseInt( Math.min(Math.max(parseInt(clientSize / 8) , 80) , self.size) ) + 1;
          var step = parseInt(clientSize / 8);
          if(step < 80) step = 80;
          if(step > self.size) step = self.size;
          step += 1;
          
          content_wrap[scroll_direction] = scrollAmount - (delta * step);


          return scrollEnd && !self.lock_anyway;
        })
      }
      
      
      //swipe not available yet
      var touchDrag = ace.vars['touch'] && 'ace_drag' in $.event.special && settings.touchDrag //&& !settings.touchSwipe;
      //add drag event for touch devices to scroll
      if(touchDrag/** || ($.fn.swipe && settings.touchSwipe)*/) {
        var dir = '', event_name = touchDrag ? 'ace_drag' : 'swipe';
        this.$element.on(event_name + '.ace_scroll', function(event) {
          if(disabled) {
            event.retval.cancel = true;
            return;
          }
          checkContentChanges(true);
          
          if(!active) {
            event.retval.cancel = this.lock_anyway;
            return;
          }

          dir = event.direction;
          if( (vertical && (dir == 'up' || dir == 'down'))
            ||
            (!vertical && (dir == 'left' || dir == 'right'))
             )
          {
            var distance = vertical ? event.dy : event.dx;

            if(distance != 0) {
              if(Math.abs(distance) > 20 && touchDrag) distance = distance * 2;

              self.move_bar(true);
              content_wrap[scroll_direction] = content_wrap[scroll_direction] + distance;
            }
          }
          
        })
      }
      
      
      /////////////////////////////////
      
      if(hideOnIdle) {
        $track.addClass('idle-hide');
      }
      if(observeContent) {
        $track.on('mouseenter.ace_scroll', function() {
          insideTrack = true;
          checkContentChanges(false);
        }).on('mouseleave.ace_scroll', function() {
          insideTrack = false;
          if(mouse_track == false) hideScrollbars();
        });
      }


      
      //some mobile browsers don't have mouseenter
      this.$element.on('mouseenter.ace_scroll touchstart.ace_scroll', function(e) {
        is_dirty = true;
        if(observeContent) checkContentChanges(true);
        else if(settings.hoverReset) self.reset(true);
        
        $track.addClass('scroll-hover');
      }).on('mouseleave.ace_scroll touchend.ace_scroll', function() {
        $track.removeClass('scroll-hover');
      });
      //

      if(!vertical) $content_wrap.children(0).css(css_size, this.size);//the extra wrapper
      $content_wrap.css(max_css_size , this.size);
      
      disabled = false;
      created = true;
    }
    this.is_active = function() {
      return active;
    }
    this.is_enabled = function() {
      return !disabled;
    }
    this.move_bar = function($move) {
      move_bar = $move;
    }
    
    this.get_track = function() {
      return track;
    }

    this.reset = function(innert_call) {
      if(disabled) return;// this;
      if(!created) this.create();
      /////////////////////
      var size = this.size;
      
      if(innert_call && !is_dirty) {
        return;
      }
      is_dirty = false;

      if(detached) {
        var border_size = parseInt(Math.round( (parseInt($content_wrap.css('border-top-width')) + parseInt($content_wrap.css('border-bottom-width'))) / 2.5 ));//(2.5 from trial?!)
        size -= border_size;//only if detached
      }
  
      var content_size   = vertical ? content_wrap[scroll_size] : size;
      if( (vertical && content_size == 0) || (!vertical && this.element.scrollWidth == 0) ) {
        //element is hidden
        //this.$element.addClass('scroll-hidden');
        $track.removeClass('scroll-active')
        return;// this;
      }

      var available_space = vertical ? size : content_wrap.clientWidth;

      if(!vertical) $content_wrap.children(0).css(css_size, size);//the extra wrapper
      $content_wrap.css(max_css_size , this.size);
      

      if(content_size > available_space) {
        active = true;
        $track.css(css_size, available_space).show();

        ratio = parseFloat((available_space / content_size).toFixed(5))
        
        bar_size = parseInt(Math.round(available_space * ratio));
        bar_size_2 = parseInt(Math.round(bar_size / 2));

        bar_max_pos = available_space - bar_size;
        bar_pos = parseInt(Math.round(content_wrap[scroll_direction] * ratio));

        bar_style[css_size] = bar_size + 'px';
        bar_style[css_pos] = bar_pos + 'px';
        
        $track.addClass('scroll-active');
        
        if(trackSize == 0) {
          getTrackSize();
        }

        if(!reset_once) {
          //this.$element.removeClass('scroll-hidden');
          if(settings.reset) {
            //reset scrollbar to zero position at first             
            content_wrap[scroll_direction] = 0;
            bar_style[css_pos] = 0;
          }
          reset_once = true;
        }
        
        if(detached) setTrackPos();
      } else {
        active = false;
        $track.hide();
        $track.removeClass('scroll-active');
        $content_wrap.css(max_css_size , '');
      }

      return;// this;
    }
    this.disable = function() {
      content_wrap[scroll_direction] = 0;
      bar_style[css_pos] = 0;

      disabled = true;
      active = false;
      $track.hide();
      
      this.$element.addClass('scroll-disabled');
      
      $track.removeClass('scroll-active');
      $content_wrap.css(max_css_size , '');
    }
    this.enable = function() {
      disabled = false;
      this.$element.removeClass('scroll-disabled');
    }
    this.destroy = function() {
      active = false;
      disabled = false;
      created = false;
      
      this.$element.removeClass('ace-scroll scroll-disabled scroll-active');
      this.$element.off('.ace_scroll')

      if(!detached) {
        if(!vertical) {
          //remove the extra wrapping div
          $content_wrap.find('> div').children().unwrap();
        }
        $content_wrap.children().unwrap();
        $content_wrap.remove();
      }
      
      $track.remove();
      
      if(inline_style !== false) this.element.style.position = inline_style;
      
      if(idleTimer != null) {
        clearTimeout(idleTimer);
        idleTimer = null;
      }
    }
    this.modify = function(_settings) {
      if(_settings) settings = $.extend({}, settings, _settings);
      
      this.destroy();
      this.create();
      is_dirty = true;
      this.reset(true);
    }
    this.update = function(_settings) {
      if(_settings) settings = $.extend({}, settings, _settings);

      this.size = settings.size || this.size;
      
      this.lock = settings.mouseWheelLock || this.lock;
      this.lock_anyway = settings.lockAnyway || this.lock_anyway;
      
      hideOnIdle = settings.hideOnIdle || hideOnIdle;
      hideDelay = settings.hideDelay || hideDelay;
      observeContent = settings.observeContent || false;
      
      dragEvent = settings.dragEvent || false;
      
      if(typeof _settings.styleClass !== 'undefined') {
        if(styleClass) $track.removeClass(styleClass);
        styleClass = _settings.styleClass;
        if(styleClass) $track.addClass(styleClass);
        trackFlip = !!styleClass.match(/scroll\-left|scroll\-top/);
      }
    }
    
    this.start = function() {
      content_wrap[scroll_direction] = 0;
    }
    this.end = function() {
      content_wrap[scroll_direction] = content_wrap[scroll_size];
    }
    
    this.hide = function() {
      $track.hide();
    }
    this.show = function() {
      $track.show();
    }

    
    this.update_scroll = function() {
      move_bar = false;
      bar_style[css_pos] = bar_pos + 'px';
      content_wrap[scroll_direction] = parseInt(Math.round(bar_pos / ratio));
    }

    function mouse_down_track(e) {
      e.preventDefault();
      e.stopPropagation();
        
      var track_offset = $track.offset();
      var track_pos = track_offset[css_pos];//top for vertical, left for horizontal
      var mouse_pos = vertical ? e.pageY : e.pageX;
      
      if(mouse_pos > track_pos + bar_pos) {
        bar_pos = mouse_pos - track_pos - bar_size + bar_size_2;
        if(bar_pos > bar_max_pos) {           
          bar_pos = bar_max_pos;
        }
      }
      else {
        bar_pos = mouse_pos - track_pos - bar_size_2;
        if(bar_pos < 0) bar_pos = 0;
      }

      self.update_scroll()
    }

    var mouse_pos1 = -1, mouse_pos2 = -1;
    function mouse_down_bar(e) {
      e.preventDefault();
      e.stopPropagation();

      if(vertical) {
        mouse_pos2 = mouse_pos1 = e.pageY;
      } else {
        mouse_pos2 = mouse_pos1 = e.pageX;
      }

      mouse_track = true;
      $('html').off('mousemove.ace_scroll').on('mousemove.ace_scroll', mouse_move_bar)
      $(mouse_release_target).off('mouseup.ace_scroll').on('mouseup.ace_scroll', mouse_up_bar);
      
      $track.addClass('active');
      if(dragEvent) self.$element.trigger('drag.start');
    }
    function mouse_move_bar(e) {
      e.preventDefault();
      e.stopPropagation();

      if(vertical) {
        mouse_pos2 = e.pageY;
      } else {
        mouse_pos2 = e.pageX;
      }
      

      if(mouse_pos2 - mouse_pos1 + bar_pos > bar_max_pos) {
        mouse_pos2 = mouse_pos1 + bar_max_pos - bar_pos;
      } else if(mouse_pos2 - mouse_pos1 + bar_pos < 0) {
        mouse_pos2 = mouse_pos1 - bar_pos;
      }
      bar_pos = bar_pos + (mouse_pos2 - mouse_pos1);

      mouse_pos1 = mouse_pos2;

      if(bar_pos < 0) {
        bar_pos = 0;
      }
      else if(bar_pos > bar_max_pos) {
        bar_pos = bar_max_pos;
      }
      
      self.update_scroll()
    }
    function mouse_up_bar(e) {
      e.preventDefault();
      e.stopPropagation();
      
      mouse_track = false;
      $('html').off('.ace_scroll')
      $(mouse_release_target).off('.ace_scroll');

      $track.removeClass('active');
      if(dragEvent) self.$element.trigger('drag.end');
      
      if(active && hideOnIdle && !insideTrack) hideScrollbars();
    }
    
    
    var idleTimer = null;
    var prevCheckTime = 0;
    function checkContentChanges(hideSoon) {
      //check if content size has been modified since last time?
      //and with at least 1s delay
      var newCheck = +new Date();
      if(observeContent && newCheck - prevCheckTime > 1000) {
        var newSize = content_wrap[scroll_size];
        if(prevContentSize != newSize) {
          prevContentSize = newSize;
          is_dirty = true;
          self.reset(true);
        }
        prevCheckTime = newCheck;
      }
      
      //show scrollbars when not idle anymore i.e. triggered by mousewheel, dragging, etc
      if(active && hideOnIdle) {
        if(idleTimer != null) {
          clearTimeout(idleTimer);
          idleTimer = null;
        }
        $track.addClass('not-idle');
      
        if(!insideTrack && hideSoon == true) {
          //hideSoon is false when mouse enters track
          hideScrollbars();
        }
      }
    }

    function hideScrollbars() {
      if(idleTimer != null) {
        clearTimeout(idleTimer);
        idleTimer = null;
      }
      idleTimer = setTimeout(function() {
        idleTimer = null;
        $track.removeClass('not-idle');
      } , hideDelay);
    }
    
    //for detached scrollbars
    function getTrackSize() {
      $track.css('visibility', 'hidden').addClass('scroll-hover');
      if(vertical) trackSize = parseInt($track.outerWidth()) || 0;
       else trackSize = parseInt($track.outerHeight()) || 0;
      $track.css('visibility', '').removeClass('scroll-hover');
    }
    this.track_size = function() {
      if(trackSize == 0) getTrackSize();
      return trackSize;
    }
    
    //for detached scrollbars
    function setTrackPos() {
      if(updatePos === false) return;
    
      var off = $content_wrap.offset();//because we want it relative to parent not document
      var left = off.left;
      var top = off.top;

      if(vertical) {
        if(!trackFlip) {
          left += ($content_wrap.outerWidth() - trackSize)
        }
      }
      else {
        if(!trackFlip) {
          top += ($content_wrap.outerHeight() - trackSize)
        }
      }
      
      if(updatePos === true) $track.css({top: parseInt(top), left: parseInt(left)});
      else if(updatePos === 'left') $track.css('left', parseInt(left));
      else if(updatePos === 'top') $track.css('top', parseInt(top));
    }
    


    this.create();
    is_dirty = true;
    this.reset(true);
    prevContentSize = content_wrap[scroll_size];

    return this;
  }

  
  $.fn.ace_scroll = function (option,value) {
    var retval;

    var $set = this.each(function () {
      var $this = $(this);
      var data = $this.data('ace_scroll');
      var options = typeof option === 'object' && option;

      if (!data) $this.data('ace_scroll', (data = new Ace_Scroll(this, options)));
       //else if(typeof options == 'object') data['modify'](options);
      //if (typeof option === 'string') retval = data[option](value);
    });

    return (retval === undefined) ? $set : retval;
  };


  $.fn.ace_scroll.defaults = {
    'size' : 200,
    'horizontal': false,
    'mouseWheel': true,
    'mouseWheelLock': false,
    'lockAnyway': false,
    'styleClass' : false,
    
    'observeContent': false,
    'hideOnIdle': false,
    'hideDelay': 1500,
    
    'hoverReset': true //reset scrollbar sizes on mouse hover because of possible sizing changes
    ,
    'reset': false //true= set scrollTop = 0
    ,
    'dragEvent': false
    ,
    'touchDrag': true
    ,
    'touchSwipe': false
    ,
    'scrollEvent': false //trigger scroll event

    ,
    'detached': false
    ,
    'updatePos': true
    /**
    ,   
    'track' : true,
    'show' : false,
    'dark': false,
    'alwaysVisible': false,
    'margin': false,
    'thin': false,
    'position': 'right'
    */
     }

  /**
  $(document).on('ace.settings.ace_scroll', function(e, name) {
    if(name == 'sidebar_collapsed') $('.ace-scroll').scroller('reset');
  });
  $(window).on('resize.ace_scroll', function() {
    $('.ace-scroll').scroller('reset');
  });
  */

})(window.jQuery);

/**
 Required. Ace's Basic File to Initiliaze Different Parts and Some Variables.
*/

//document ready function
jQuery(function($) {
  try {
  ace.demo.init();
  } catch(e) {}
});


//some basic variables
(function(undefined) {
  if( !('ace' in window) ) window['ace'] = {}
  if( !('helper' in window['ace']) ) window['ace'].helper = {}
  if( !('vars' in window['ace']) ) window['ace'].vars = {}
  window['ace'].vars['icon'] = ' ace-icon ';
  window['ace'].vars['.icon'] = '.ace-icon';

  ace.vars['touch'] = ('ontouchstart' in window);//(('ontouchstart' in document.documentElement) || (window.DocumentTouch && document instanceof DocumentTouch));
  
  //sometimes the only good way to work around browser's pecularities is to detect them using user-agents
  //though it's not accurate
  var agent = navigator.userAgent
  ace.vars['webkit'] = !!agent.match(/AppleWebKit/i)
  ace.vars['safari'] = !!agent.match(/Safari/i) && !agent.match(/Chrome/i);
  ace.vars['android'] = ace.vars['safari'] && !!agent.match(/Android/i)
  ace.vars['ios_safari'] = !!agent.match(/OS ([4-9])(_\d)+ like Mac OS X/i) && !agent.match(/CriOS/i)
  
  ace.vars['ie'] = window.navigator.msPointerEnabled || (document.all && document.querySelector);//8-11
  ace.vars['old_ie'] = document.all && !document.addEventListener;//8 and below
  ace.vars['very_old_ie'] = document.all && !document.querySelector;//7 and below
  ace.vars['firefox'] = 'MozAppearance' in document.documentElement.style;
  
  ace.vars['non_auto_fixed'] = ace.vars['android'] || ace.vars['ios_safari'];
  
  
  //sometimes we try to use 'tap' event instead of 'click' if jquery mobile plugin is available
  ace['click_event'] = ace.vars['touch'] && jQuery.fn.tap ? 'tap' : 'click';
})();



(function($ , undefined) {

  ace.demo = {
    functions: {},
    
    init: function(initAnyway) {
      //initAnyway used to make sure the call is from our RequireJS app and not a document ready event!
      var initAnyway = !!initAnyway && true;
      if(typeof requirejs !== "undefined" && !initAnyway) return;
      
      for(var func in ace.demo.functions) if(ace.demo.functions.hasOwnProperty(func)) {
        ace.demo.functions[func]();
      }
    }
  }


  ace.demo.functions.basics = function() {
    // for android and ios we don't use "top:auto" when breadcrumbs is fixed
    if(ace.vars['non_auto_fixed']) {
      $('body').addClass('mob-safari');
    }

    ace.vars['transition'] = ace.vars['animation'] || !!$.support.transition;
  }
  
  ace.demo.functions.enableSidebar = function() {
    //initiate sidebar function
    var $sidebar = $('.sidebar');
    if($.fn.ace_sidebar) $sidebar.ace_sidebar();
    if($.fn.ace_sidebar_scroll) $sidebar.ace_sidebar_scroll({
      //for other options please see documentation
      'include_toggle': false || ace.vars['safari'] || ace.vars['ios_safari'] //true = include toggle button in the scrollbars
    });
    if($.fn.ace_sidebar_hover)  $sidebar.ace_sidebar_hover({
      'sub_hover_delay': 750,
      'sub_scroll_style': 'no-track scroll-thin scroll-margin scroll-visible'
    });
  }

  
  //Load content via ajax
  ace.demo.functions.enableDemoAjax = function() {
    if(!$.fn.ace_ajax) return;
 
    if(window.Pace) {
      window.paceOptions = {
        ajax: true,
        document: true,
        eventLag: false // disabled
        //elements: {selectors: ['.page-content-area']}
      }
    }

    var demo_ajax_options = {
       'close_active': true,
       
       close_mobile_menu: '#sidebar',
       close_dropdowns: true,
       
       'default_url': 'page/index',//default hash
       'content_url': function(hash) {
        //***NOTE***
        //this is for Ace demo only, you should change it to return a valid URL
        //please refer to documentation for more info

        if( !hash.match(/^page\//) ) return false;
        var path = document.location.pathname;

        //for example in Ace HTML demo version we convert /ajax/index.html#page/gallery to > /ajax/content/gallery.html and load it
        if(path.match(/(\/ajax\/)(index\.html)?/))
          return path.replace(/(\/ajax\/)(index\.html)?/, '/ajax/content/'+hash.replace(/^page\//, '')+'.html') ;

        //for example in Ace PHP demo version we convert "ajax.php#page/dashboard" to "ajax.php?page=dashboard" and load it
        return path + "?" + hash.replace(/\//, "=");
        }       
    }
       
    //for IE9 and below we exclude PACE loader (using conditional IE comments)
    //for other browsers we use the following extra ajax loader options
    if(window.Pace) {
      demo_ajax_options['loading_overlay'] = 'body';//the opaque overlay is applied to 'body'
    }

    //initiate ajax loading on this element( which is .page-content-area[data-ajax-content=true] in Ace's demo)
    $('[data-ajax-content=true]').ace_ajax(demo_ajax_options)

    //if general error happens and ajax is working, let's stop loading icon & PACE
    $(window).on('error.ace_ajax', function() {
      $('[data-ajax-content=true]').each(function() {
        var $this = $(this);
        if( $this.ace_ajax('working') ) {
          if(window.Pace && Pace.running) Pace.stop();
          $this.ace_ajax('stopLoading', true);
        }
      })
    })
  }

  /////////////////////////////

  ace.demo.functions.handleScrollbars = function() {
    //add scrollbars for navbar dropdowns
    var has_scroll = !!$.fn.ace_scroll;
    if(has_scroll) $('.dropdown-content').ace_scroll({reset: false, mouseWheelLock: true})

    //reset scrolls bars on window resize
    if(has_scroll && !ace.vars['old_ie']) {//IE has an issue with widget fullscreen on ajax?!!!
      $(window).on('resize.reset_scroll', function() {
        $('.ace-scroll:not(.scroll-disabled)').not(':hidden').ace_scroll('reset');
      });
      if(has_scroll) $(document).on('settings.ace.reset_scroll', function(e, name) {
        if(name == 'sidebar_collapsed') $('.ace-scroll:not(.scroll-disabled)').not(':hidden').ace_scroll('reset');
      });
    }
  }


  ace.demo.functions.dropdownAutoPos = function() {
    //change a dropdown to "dropup" depending on its position
    $(document).on('click.dropdown.pos', '.dropdown-toggle[data-position="auto"]', function() {
      var offset = $(this).offset();
      var parent = $(this.parentNode);

      if ( parseInt(offset.top + $(this).height()) + 50 
          >
        (ace.helper.scrollTop() + ace.helper.winHeight() - parent.find('.dropdown-menu').eq(0).height()) 
        ) parent.addClass('dropup');
      else parent.removeClass('dropup');
    });
  }

  
  ace.demo.functions.navbarHelpers = function() {
    //prevent dropdowns from hiding when a from is clicked
    /**$(document).on('click', '.dropdown-navbar form', function(e){
      e.stopPropagation();
    });*/


    //disable navbar icon animation upon click
    $('.ace-nav [class*="icon-animated-"]').closest('a').one('click', function(){
      var icon = $(this).find('[class*="icon-animated-"]').eq(0);
      var $match = icon.attr('class').match(/icon\-animated\-([\d\w]+)/);
      icon.removeClass($match[0]);
    });


    //prevent dropdowns from hiding when a tab is selected
    $(document).on('click', '.dropdown-navbar .nav-tabs', function(e){
      e.stopPropagation();
      var $this , href
      var that = e.target
      if( ($this = $(e.target).closest('[data-toggle=tab]')) && $this.length > 0) {
        $this.tab('show');
        e.preventDefault();
        $(window).triggerHandler('resize.navbar.dropdown')
      }
    });
  }

  
  ace.demo.functions.sidebarTooltips = function() {
    //tooltip in sidebar items
    $('.sidebar .nav-list .badge[title],.sidebar .nav-list .badge[title]').each(function() {
      var tooltip_class = $(this).attr('class').match(/tooltip\-(?:\w+)/);
      tooltip_class = tooltip_class ? tooltip_class[0] : 'tooltip-error';
      $(this).tooltip({
        'placement': function (context, source) {
          var offset = $(source).offset();

          if( parseInt(offset.left) < parseInt(document.body.scrollWidth / 2) ) return 'right';
          return 'left';
        },
        container: 'body',
        template: '<div class="tooltip '+tooltip_class+'"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
      });
    });
    
    //or something like this if items are dynamically inserted
    /**
    $('.sidebar').tooltip({
      'placement': function (context, source) {
        var offset = $(source).offset();

        if( parseInt(offset.left) < parseInt(document.body.scrollWidth / 2) ) return 'right';
        return 'left';
      },
      selector: '.nav-list .badge[title],.nav-list .label[title]',
      container: 'body',
      template: '<div class="tooltip tooltip-error"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
    });
    */
  }
  
  

  ace.demo.functions.scrollTopBtn = function() {
    //the scroll to top button
    var scroll_btn = $('.btn-scroll-up');
    if(scroll_btn.length > 0) {
      var is_visible = false;
      $(window).on('scroll.scroll_btn', function() {
        var scroll = ace.helper.scrollTop();
        var h = ace.helper.winHeight();
        var body_sH = document.body.scrollHeight;
        if(scroll > parseInt(h / 4) || (scroll > 0 && body_sH >= h && h + scroll >= body_sH - 1)) {//|| for smaller pages, when reached end of page
          if(!is_visible) {
            scroll_btn.addClass('display');
            is_visible = true;
          }
        } else {
          if(is_visible) {
            scroll_btn.removeClass('display');
            is_visible = false;
          }
        }
      }).triggerHandler('scroll.scroll_btn');

      scroll_btn.on(ace.click_event, function(){
        var duration = Math.min(500, Math.max(100, parseInt(ace.helper.scrollTop() / 3)));
        $('html,body').animate({scrollTop: 0}, duration);
        return false;
      });
    }
  }


  
  ace.demo.functions.someBrowserFix = function() {
    //chrome and webkit have a problem here when resizing from 479px to more
    //we should force them redraw the navbar!
    if( ace.vars['webkit'] ) {
      var ace_nav = $('.ace-nav').get(0);
      if( ace_nav ) $(window).on('resize.webkit_fix' , function(){
        ace.helper.redraw(ace_nav);
      });
    }
    
    
    //fix an issue with ios safari, when an element is fixed and an input receives focus
    if(ace.vars['ios_safari']) {
      $(document).on('ace.settings.ios_fix', function(e, event_name, event_val) {
      if(event_name != 'navbar_fixed') return;

      $(document).off('focus.ios_fix blur.ios_fix', 'input,textarea,.wysiwyg-editor');
      if(event_val == true) {
        $(document).on('focus.ios_fix', 'input,textarea,.wysiwyg-editor', function() {
        $(window).on('scroll.ios_fix', function() {
          var navbar = $('#navbar').get(0);
          if(navbar) ace.helper.redraw(navbar);
        });
        }).on('blur.ios_fix', 'input,textarea,.wysiwyg-editor', function() {
        $(window).off('scroll.ios_fix');
        })
      }
      }).triggerHandler('ace.settings.ios_fix', ['navbar_fixed', $('#navbar').css('position') == 'fixed']);
    }
  }

  
  
  ace.demo.functions.bsCollapseToggle = function() {
    //bootstrap collapse component icon toggle
    $(document).on('hide.bs.collapse show.bs.collapse', function (ev) {
      var panel_id = ev.target.getAttribute('id')
      var panel = $('a[href*="#'+ panel_id+'"]');
      if(panel.length == 0) panel = $('a[data-target*="#'+ panel_id+'"]');
      if(panel.length == 0) return;

      panel.find(ace.vars['.icon']).each(function(){
        var $icon = $(this)

        var $match
        var $icon_down = null
        var $icon_up = null
        if( ($icon_down = $icon.attr('data-icon-show')) ) {
          $icon_up = $icon.attr('data-icon-hide')
        }
        else if( $match = $icon.attr('class').match(/fa\-(.*)\-(up|down)/) ) {
          $icon_down = 'fa-'+$match[1]+'-down'
          $icon_up = 'fa-'+$match[1]+'-up'
        }

        if($icon_down) {
          if(ev.type == 'show') $icon.removeClass($icon_down).addClass($icon_up)
            else $icon.removeClass($icon_up).addClass($icon_down)
            
          return false;//ignore other icons that match, one is enough
        }

      });
    })
  }
  

  
  //in small devices display navbar dropdowns like modal boxes
  ace.demo.functions.smallDeviceDropdowns = function() {
    if(ace.vars['old_ie']) return;
    
    $(document)
    .on('shown.bs.dropdown.navbar', '.ace-nav > li.dropdown-modal', function(e) {
    adjustNavbarDropdown.call(this);
    var self = this;
    $(window).on('resize.navbar.dropdown', function() {
      adjustNavbarDropdown.call(self);
    })
    })
    .on('hidden.bs.dropdown.navbar', '.ace-nav > li.dropdown-modal', function(e) {
    $(window).off('resize.navbar.dropdown');
    resetNavbarDropdown.call(this);
    })
   
    function adjustNavbarDropdown() {
    var $sub = $(this).find('> .dropdown-menu');

    if( $sub.css('position') == 'fixed' ) {
      var win_width = parseInt($(window).width());
      var offset_w = win_width > 320 ? 60 : (win_width > 240 ? 40 : 30);
      var avail_width = parseInt(win_width) - offset_w;
      var avail_height = parseInt($(window).height()) - 30;
      
      var width = parseInt(Math.min(avail_width , 320));
      //we set 'width' here for text wrappings and spacings to take effect before calculating scrollHeight
      $sub.css('width', width);

      var tabbed = false;
      var extra_parts = 0;
      var dropdown_content = $sub.find('.tab-pane.active .dropdown-content.ace-scroll');
      if(dropdown_content.length == 0) dropdown_content = $sub.find('.dropdown-content.ace-scroll');
      else tabbed = true;

      var parent_menu = dropdown_content.closest('.dropdown-menu');
      var scrollHeight = $sub[0].scrollHeight;
      if(dropdown_content.length == 1) {
        //sometimes there's no scroll-content, for example in detached scrollbars
        var content =  dropdown_content.find('.scroll-content')[0];
        if(content) {
          scrollHeight = content.scrollHeight;
        }
      
        extra_parts += parent_menu.find('.dropdown-header').outerHeight();
        extra_parts += parent_menu.find('.dropdown-footer').outerHeight();
        
        var tab_content = parent_menu.closest('.tab-content');
        if( tab_content.length != 0 ) {
          extra_parts += tab_content.siblings('.nav-tabs').eq(0).height();
        }
      }
      

      
      var height = parseInt(Math.min(avail_height , 480, scrollHeight + extra_parts));
      var left = parseInt(Math.abs((avail_width + offset_w - width)/2));
      var top = parseInt(Math.abs((avail_height + 30 - height)/2));

      
      var zindex = parseInt($sub.css('z-index')) || 0;

      $sub.css({'height': height, 'left': left, 'right': 'auto', 'top': top - (!tabbed ? 1 : 3)});
      if(dropdown_content.length == 1) {
        if(!ace.vars['touch']) {
          dropdown_content.ace_scroll('update', {size: height - extra_parts}).ace_scroll('enable').ace_scroll('reset');
        }
        else {
          dropdown_content
          .ace_scroll('disable').css('max-height', height - extra_parts).addClass('overflow-scroll');
        }
      }
      $sub.css('height', height + (!tabbed ? 2 : 7));//for bottom border adjustment and tab content paddings
      
      
      if($sub.hasClass('user-menu')) {
        $sub.css('height', '');//because of user-info hiding/showing at different widths, which changes above 'scrollHeight', so we remove it!
        
        //user menu is re-positioned in small widths
        //but we need to re-position again in small heights as well (modal mode)
        var user_info = $(this).find('.user-info');
        if(user_info.length == 1 && user_info.css('position') == 'fixed') {
          user_info.css({'left': left, 'right': 'auto', 'top': top, 'width': width - 2, 'max-width': width - 2, 'z-index': zindex + 1});
        }
        else user_info.css({'left': '', 'right': '', 'top': '', 'width': '', 'max-width': '', 'z-index': ''});
      }
      
      //dropdown's z-index is limited by parent .navbar's z-index (which doesn't make sense because dropdowns are fixed!)
      //so for example when in 'content-slider' page, fixed modal toggle buttons go above are dropdowns
      //so we increase navbar's z-index to fix this!
      $(this).closest('.navbar.navbar-fixed-top').css('z-index', zindex);
    }
    else {
      if($sub.length != 0) resetNavbarDropdown.call(this, $sub);
    }
    }

    //reset scrollbars and user menu
    function resetNavbarDropdown($sub) {
    $sub = $sub || $(this).find('> .dropdown-menu');
    
      if($sub.length > 0) {
      $sub
      .css({'width': '', 'height': '', 'left': '', 'right': '', 'top': ''})
      .find('.dropdown-content').each(function() {
        if(ace.vars['touch']) {
          $(this).css('max-height', '').removeClass('overflow-scroll');
        }

        var size = parseInt($(this).attr('data-size') || 0) || $.fn.ace_scroll.defaults.size;
        $(this).ace_scroll('update', {size: size}).ace_scroll('enable').ace_scroll('reset');
      })
      
      if( $sub.hasClass('user-menu') ) {
        var user_info = 
        $(this).find('.user-info')
        .css({'left': '', 'right': '', 'top': '', 'width': '', 'max-width': '', 'z-index': ''});
      }
    }
    
    $(this).closest('.navbar').css('z-index', '');
    }
  }

})(jQuery);


//some ace helper functions
(function($$ , undefined) {//$$ is ace.helper
 $$.unCamelCase = function(str) {
  return str.replace(/([a-z])([A-Z])/g, function(match, c1, c2){ return c1+'-'+c2.toLowerCase() })
 }
 $$.strToVal = function(str) {
  var res = str.match(/^(?:(true)|(false)|(null)|(\-?[\d]+(?:\.[\d]+)?)|(\[.*\]|\{.*\}))$/i);

  var val = str;
  if(res) {
    if(res[1]) val = true;
    else if(res[2]) val = false;
    else if(res[3]) val = null; 
    else if(res[4]) val = parseFloat(str);
    else if(res[5]) {
      try { val = JSON.parse(str) }
      catch (err) {}
    }
  }

  return val;
 }
 $$.getAttrSettings = function(elem, attr_list, prefix) {
  if(!elem) return;
  var list_type = attr_list instanceof Array ? 1 : 2;
  //attr_list can be Array or Object(key/value)
  var prefix = prefix ? prefix.replace(/([^\-])$/ , '$1-') : '';
  prefix = 'data-' + prefix;

  var settings = {}
  for(var li in attr_list) if(attr_list.hasOwnProperty(li)) {
    var name = list_type == 1 ? attr_list[li] : li;
    var attr_val, attr_name = $$.unCamelCase(name.replace(/[^A-Za-z0-9]{1,}/g , '-')).toLowerCase()

    if( ! ((attr_val = elem.getAttribute(prefix + attr_name))  ) ) continue;
    settings[name] = $$.strToVal(attr_val);
  }

  return settings;
 }

 $$.scrollTop = function() {
  return document.scrollTop || document.documentElement.scrollTop || document.body.scrollTop
 }
 $$.winHeight = function() {
  return window.innerHeight || document.documentElement.clientHeight;
 }
 $$.redraw = function(elem, force) {
  if(!elem) return;
  var saved_val = elem.style['display'];
  elem.style.display = 'none';
  elem.offsetHeight;
  if(force !== true) {
    elem.style.display = saved_val;
  }
  else {
    //force redraw for example in old IE
    setTimeout(function() {
      elem.style.display = saved_val;
    }, 10);
  }
 }
})(ace.helper);